# ----------------------------------------------------------------------------------------------------------------------
# REQUIRE A SPECIFIC TERRAFORM VERSION OR HIGHER
# This module has been updated with 0.12 syntax, which means it is no longer compatible with any versions below 0.12.
# ----------------------------------------------------------------------------------------------------------------------
provider "aws"{
    region = var.aws_region
}
terraform {
  # This module is now only being tested with Terraform 0.13.x. However, to make upgrading easier, we are setting
  # 0.12.26 as the minimum version, as that version added support for required_providers with source URLs, making it
  # forwards compatible with 0.13.x code.
  backend "s3" {}
  required_version = ">= 0.12.26"
}

# ----------------------------------------------------------------------------------------------------------------------
# CREATE THE S3 BUCKET
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_s3_bucket" "bucket" {
  count = var.create_resources ? 1 : 0

  bucket              = var.name
  acl                 = var.acl
  acceleration_status = var.acceleration_status
  request_payer       = var.request_payer
  force_destroy       = var.force_destroy
  tags                = var.tags

  # Always enable server-side encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        # If a KMS key is not provided (kms_key_arn is null), the default aws/s3 key is used
        kms_master_key_id = var.kms_key_arn
        sse_algorithm     = var.sse_algorithm
      }
    }
  }

  # Optionally enable versioning. If enabled, instead of overriding objects, the S3 bucket will always create a new
  # version of each object, so all the old values are retained.
  versioning {
    enabled    = var.enable_versioning
    mfa_delete = var.mfa_delete
  }

  # Optionally enable access logging
  dynamic "logging" {
    for_each = var.access_logging_enabled ? ["once"] : []
    content {
      target_bucket = var.access_logging_bucket
      target_prefix = var.access_logging_prefix
    }
  }

  # Optionally enable replication
  dynamic "replication_configuration" {
    for_each = var.replication_enabled ? ["once"] : []
    content {
      role = var.replication_role

      dynamic "rules" {
        for_each = var.replication_rules
        content {
          id       = rules.key
          status   = lookup(rules.value, "status")
          priority = lookup(rules.value, "priority", null)
          prefix   = lookup(rules.value, "prefix", null)

          destination {
            bucket             = lookup(rules.value, "destination_bucket")
            storage_class      = lookup(rules.value, "destination_storage_class", null)
            replica_kms_key_id = lookup(rules.value, "destination_replica_kms_key_id", null)
            account_id         = lookup(rules.value, "destination_account_id", null)

            dynamic "access_control_translation" {
              for_each = lookup(rules.value, "destination_access_control_translation", false) ? ["once"] : []
              content {
                owner = "Destination"
              }
            }
          }

          dynamic "source_selection_criteria" {
            for_each = lookup(rules.value, "source_selection_criteria_enabled", false) ? ["once"] : []

            content {
              sse_kms_encrypted_objects {
                enabled = true
              }
            }
          }

          dynamic "filter" {
            for_each = lookup(rules.value, "filter", {})

            content {
              prefix = lookup(filter.value, "prefix", null)
              tags   = lookup(filter.value, "tags", null)
            }
          }
        }
      }
    }
  }

  # Optionally enable object locking. This can be used to prevent deleting objects in this bucket for a customizable
  # period of time.
  dynamic "object_lock_configuration" {
    for_each = var.object_lock_enabled ? ["once"] : []
    content {
      object_lock_enabled = "Enabled"

      rule {
        default_retention {
          mode  = var.object_lock_mode
          days  = var.object_lock_days
          years = var.object_lock_years
        }
      }
    }
  }

  # Optionally enable lifecycle rules. These can be used to switch storage types or delete objects based on customizable
  # rules.
  dynamic "lifecycle_rule" {
    for_each = var.lifecycle_rules
    content {
      id                                     = lifecycle_rule.key
      enabled                                = lookup(lifecycle_rule.value, "enabled")
      prefix                                 = lookup(lifecycle_rule.value, "prefix", null)
      tags                                   = lookup(lifecycle_rule.value, "tags", null)
      abort_incomplete_multipart_upload_days = lookup(lifecycle_rule.value, "abort_incomplete_multipart_upload_days", null)

      dynamic "expiration" {
        for_each = lookup(lifecycle_rule.value, "expiration", {})
        content {
          date                         = lookup(expiration.value, "date", null)
          days                         = lookup(expiration.value, "days", null)
          expired_object_delete_marker = lookup(expiration.value, "expired_object_delete_marker ", null)
        }
      }

      dynamic "transition" {
        for_each = lookup(lifecycle_rule.value, "transition", {})
        content {
          storage_class = lookup(transition.value, "storage_class")
          date          = lookup(transition.value, "date", null)
          days          = lookup(transition.value, "days", null)
        }
      }

      dynamic "noncurrent_version_expiration" {
        for_each = lookup(lifecycle_rule.value, "noncurrent_version_expiration", null) != null ? ["once"] : []
        content {
          days = lookup(lifecycle_rule.value, "noncurrent_version_expiration")
        }
      }

      dynamic "noncurrent_version_transition" {
        for_each = lookup(lifecycle_rule.value, "noncurrent_version_transition", {})
        content {
          days          = lookup(noncurrent_version_transition.value, "days")
          storage_class = lookup(noncurrent_version_transition.value, "storage_class")
        }
      }
    }
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# BLOCK ALL POSSIBILITY OF ACCIDENTALLY ENABLING PUBLIC ACCESS TO THIS BUCKET
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_s3_bucket_public_access_block" "public_access" {
  count = var.create_resources ? 1 : 0

  bucket                  = aws_s3_bucket.bucket[0].id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# ----------------------------------------------------------------------------------------------------------------------
# CREATE A BUCKET POLICY TO CONTROL ACCESS TO THE BUCKET
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_s3_bucket_policy" "bucket_policy" {
  count = var.create_resources ? 1 : 0

  bucket = aws_s3_bucket.bucket[0].id
  policy = data.aws_iam_policy_document.config_bucket_policy[0].json

  depends_on = [aws_s3_bucket_public_access_block.public_access]
}

data "aws_iam_policy_document" "config_bucket_policy" {
  count = var.create_resources ? 1 : 0

  # Users can provide custom rules for what permissions to grant to this bucket
  dynamic "statement" {
    for_each = var.bucket_policy_statements
    content {
      sid         = statement.key
      effect      = lookup(statement.value, "effect", null)
      actions     = lookup(statement.value, "actions", null)
      not_actions = lookup(statement.value, "not_actions", null)
      resources   = [for key in lookup(statement.value, "keys", [""]) : "${aws_s3_bucket.bucket[0].arn}${key}"]

      dynamic "principals" {
        for_each = lookup(statement.value, "principals", {})
        content {
          type        = principals.key
          identifiers = principals.value
        }
      }

      dynamic "not_principals" {
        for_each = lookup(statement.value, "not_principals", {})
        content {
          type        = not_principals.key
          identifiers = not_principals.value
        }
      }

      dynamic "condition" {
        for_each = lookup(statement.value, "condition", {})
        content {
          test     = condition.value.test
          variable = condition.value.variable
          values   = condition.value.values
        }
      }
    }
  }

  # The only rule we include by default is to require that all access to this bucket is over TLS
  statement {
    sid     = "AllowTLSRequestsOnly"
    effect  = "Deny"
    actions = ["s3:*"]
    resources = [
      aws_s3_bucket.bucket[0].arn,
      "${aws_s3_bucket.bucket[0].arn}/*"
    ]
    principals {
      type        = "*"
      identifiers = ["*"]
    }
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["false"]
    }
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# CONFIGURE WHO OWNS OBJECTS IN THE S3 BUCKET
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_s3_bucket_ownership_controls" "bucket" {
  count = var.bucket_ownership == null ? 0 : 1

  bucket = aws_s3_bucket.bucket[0].id

  rule {
    object_ownership = var.bucket_ownership
  }
}
