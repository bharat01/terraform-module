output "name" {
  description = "The name of the S3 bucket."
  value       = length(aws_s3_bucket.bucket) > 0 ? aws_s3_bucket.bucket[0].bucket : null
}

output "arn" {
  description = "The ARN of the S3 bucket."
  value       = length(aws_s3_bucket.bucket) > 0 ? aws_s3_bucket.bucket[0].arn : null
}

output "bucket_domain_name" {
  description = "The bucket domain name. Will be of format bucketname.s3.amazonaws.com."
  value       = length(aws_s3_bucket.bucket) > 0 ? aws_s3_bucket.bucket[0].bucket_domain_name : null
}

output "bucket_regional_domain_name" {
  description = "The bucket region-specific domain name. The bucket domain name including the region name, please refer here for format. Note: The AWS CloudFront allows specifying S3 region-specific endpoint when creating S3 origin, it will prevent redirect issues from CloudFront to S3 Origin URL."
  value       = length(aws_s3_bucket.bucket) > 0 ? aws_s3_bucket.bucket[0].bucket_regional_domain_name : null
}

output "hosted_zone_id" {
  description = "The Route 53 Hosted Zone ID for this bucket's region."
  value       = length(aws_s3_bucket.bucket) > 0 ? aws_s3_bucket.bucket[0].hosted_zone_id : null
}
